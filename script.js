// CRIAR UM  BUTTON

var btnBuscar = document.getElementById("btnBuscar");
btnBuscar.onclick = function () {
    buscarApi();
};
// PEGAR INFORMAÇÕES DOS INPUTS

//  extrair informações da API

function buscarApi() {
    const diaIda = document.getElementById("idIda").value;
    var select = document.getElementById("idLocalIda");
    var ida = select.options[select.selectedIndex].value;
    var select2 = document.getElementById("idLocalDestino");
    var volta = select2.options[select2.selectedIndex].value;

    fetch(
        "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browsequotes/v1.0/BR/BRL/pt-BR/" +
        ida +
        "/" +
        volta +
        "/" +
        diaIda,
        {
            method: "GET",
            headers: {
                "x-rapidapi-host":
                    "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
                "x-rapidapi-key":
                    "69b9b48ca9mshe5277bcadcdea89p17034djsnfea8c61f9eff",
            },
        }
    )
        .then((response) => {
            return response.json();
        })
        .then(function (dado) {
            console.log(dado);
            document.getElementById("aviao").value =
                "Linha aerea: " + dado.Carriers[0].Name;

            document.getElementById("dataIda").value =
                "Data de Partida: " + dado.Quotes[0].OutboundLeg.DepartureDate;

            document.getElementById("nomeIda").value =
                "Local de origem: " + dado.Places[0].Name;

            document.getElementById("nomeVolta").value =
                "Local de destino: " + dado.Places[1].Name;

            document.getElementById("valor").value =
                "Valor: " + dado.Currencies[0].Symbol + dado.Quotes[0].MinPrice;

            console.log(dado);
        })
        .catch((err) => {
            console.error(err);
        });
}